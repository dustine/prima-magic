# [prima-magic](https://dustine.gitlab.io/prima-magic/)
## Build Setup
```bash
# install dependencies
$ pnpm install

# serve with hot reload at localhost:3000
$ pnpm run dev

# build for production and launch server
$ pnpm run build
$ pnpm run start

# generate static project
$ pnpm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## To-do list
- Test to see if probabilities are right
- Load/unload code from file + presets?
- Make things even prettier
- Add a better placeholder "string"
- Documentation