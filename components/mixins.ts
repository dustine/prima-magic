import Vue from 'vue'
import Component from 'vue-class-component'
import { Prompt, Roll } from '~/assets/randomizer'

@Component 
export class MixinPrompt extends Vue {
  getSegmentType(type: any): string {
    if (type instanceof Prompt) {
      return 'SegmentPrompt'
    } 
    if (type instanceof Roll) {
      return 'SegmentRoll'
    }
    return 'SegmentString'
  }
}