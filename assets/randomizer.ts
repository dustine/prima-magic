const defaultSeedType = 'roll'

let dict: Map<string, string[]>
let seedType: string | undefined

function notEmpty<TValue>(value: TValue | null | undefined): value is TValue {
  return value !== null && value !== undefined
}

// function processCode(code:string) {
//     const lineExp = /^([\d]*)\s*([a-z].*):\s*(.+)$/gim
//     seedType = undefined
//     dict = new Map()

//     for (const line of code.matchAll(lineExp)) {
//         const number = Math.max(1, +line[1])
//         const type = line[2].trim()
//         const roll = line[3].trim()
//         if (!seedType) seedType = type
//         dict.set(type, (dict.get(type) ?? []).concat(Array(number).fill(roll)))
//     }
// }

function processCode(code: string) {
  const lineExp = /^(\d*)(\D.*?)?:(.*)?$/i
  seedType = undefined
  dict = new Map()

  let defaultType = defaultSeedType
  for (const line of code.split('\n')) {
    let number = 1
    let type
    let entry

    const match = line.trim().match(lineExp)
    if (match) {
      if (match[1]) {
        number = +match[1]
      }
      if (match[2]) {
        // set default type if there's no roll
        if (match[3]) {
          type = match[2].trim()
        } else {
          defaultType = match[2].trim()
        }
      }
      if (match[3]) {
        entry = match[3].trim()
      }
    } else {
      entry = line.trim()
    }

    if (!type) {
      type = defaultType
    }
    if (!seedType) {
      seedType = type
    }
    if (entry) {
      dict.set(type, (dict.get(type) ?? []).concat(Array(number).fill(entry)))
    }
  }
}

function weightedRandom(
  choices: { weight: number; type: string }[]
): { type: string; roll: number } {
  const total = choices.reduce((prev, curr) => prev + curr.weight, 0)
  let roll = Math.floor(Math.random() * total)

  for (const c of choices) {
    const size = c.weight
    if (roll < size) {
      return { type: c.type, roll }
    }
    roll -= size
  }

  throw new Error(`roll ${roll + total} bigger than total weight`)
}

class Prompt {
  static maxSegments = 100
  private _segments: number = 0
  protected get segments() {
    return this._segments
  }
  protected set segments(segments: number) {
    const diff = segments - this._segments
    if (diff == 0) return
    if (this.root) {
      this.root.segments += diff
    }
    this._segments = segments
  }
  public result: (string | Prompt | Roll)[]

  constructor(protected type: string, protected root?: Prompt) {
    this.result = [`[${type}]`]
  }

  static pickEntry(type: string) {
    const typeExp = /(\d*)\s*(.+)/

    // roll between weighted categories
    const mixedChoices: { weight: number; type: string }[] = []
    for (const match of type
      .split(',')
      .map((e) => e.trim().match(typeExp))
      .filter(notEmpty)) {
      const weight = Math.max(1, +match[1])
      mixedChoices.push({ weight, type: match[2] })
    }
    if (!mixedChoices || mixedChoices.length == 0) return
    const mixedType = weightedRandom(mixedChoices).type

    // roll between summed types (with possible weights)
    let joinedChoices: { weight: number; type: string }[] = []
    for (const match of mixedType
      .split('+')
      .map((e) => e.trim().match(typeExp))
      .filter(notEmpty)) {
      const entries = dict.get(match[2])
      if (entries) {
        const times = Math.max(1, +match[1])
        joinedChoices = joinedChoices.concat(
          Array(times).fill({ weight: entries.length, type: match[2] })
        )
      }
    }
    if (!joinedChoices || joinedChoices.length == 0) return
    const choice = weightedRandom(joinedChoices)

    return (dict.get(choice.type) ?? [])[choice.roll]
  }

  roll(): this {
    const segmentExp = /([^\[\]{}]+)|\[([^\[\]]+)\]|{([^{}]+)}/g
    // TODO Subrerolls ignore gained segments from children
    this.segments = 0
    if (this.root && this.root.segments > Prompt.maxSegments) return this

    // stage 1: roll for entry
    const entry = Prompt.pickEntry(this.type)
    if (!entry) return this

    // stage 2: separate into segments (and roll if necessary)
    this.result = []
    for (const segment of entry.matchAll(segmentExp)) {
      if (segment[1]) {
        // this.segments++
        this.result.push(segment[1])
      }
      if (segment[2]) {
        this.segments++
        this.result.push(new Prompt(segment[2], this.root ?? this).roll())
      }
      if (segment[3]) {
        // this.segments++
        this.result.push(new Roll(segment[3]))
      }
    }

    if (!this.root) {
      this.capitalizeStart()
      this.dotEnd()
    }

    return this
  }

  public capitalizeStart() {
    const start = this.result[0]
    if (start instanceof Roll) return
    else if (start instanceof Prompt) start.capitalizeStart()
    else {
      this.result[0] = start[0].toUpperCase() + start.slice(1)
    }
  }

  public dotEnd() {
    const string = this.toString()
    const end = string[string.length - 1]

    if (end != '.' && end != '!' && end != '?') {
      this.result.push('.')
    }
  }

  public clone(root?: Prompt): Prompt {
    let clone = new Prompt(this.type, root)
    for (const segment of this.result) {
      if (segment instanceof Prompt) {
        clone.result.push(segment.clone(clone))
      } else if (segment instanceof Roll) {
        clone.result.push(segment.clone())
      } else {
        clone.result.push(segment)
      }
    }
    return clone
  }

  public toString(roll: boolean = false): string {
    let result = this.result.reduce(function (prev: string, curr) {
      return prev + curr.toString(roll)
    }, '')
    // result += result[result.length - 1] == '.' ? '' : '.'
    return result
  }
}

class Roll {
  public result?: number

  constructor(protected type: string) {
    if(type.match(/^\s*\d+\s*$/)) this.type = `d${this.type}`
    this.type = this.type.replaceAll(/\s+/g, '')
    this.roll()
  }

  public roll() {
    const rollExp = /(\d+\*)?(\d+)?d(\d+)([\+-]\d+)?/i
    const match = this.type.match(rollExp)
    if (match) {
      this.result = 0
      const multi = parseInt(match[1] ?? 1)
      const number = parseInt(match[2] ?? 1)
      const dice = parseInt(match[3])
      const add = parseInt(match[4] ?? 0)
      for (let index = 0; index < number; index++) {
        this.result += (Math.floor(Math.random() * dice) + 1) * multi
      }
      this.result += add
    }
  }

  public clone() {
    return new Roll(this.type)
  }

  public toString(roll: boolean = false) {
    return roll ? this.result ?? this.type : this.type
  }
}

function getPrompt() {
  return new Prompt(seedType ?? defaultSeedType).roll()
}

export { processCode, getPrompt, Prompt, Roll }
