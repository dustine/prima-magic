export const state = () => ({
  dice: false,
  code: `you rolled a {20}
you land on your head and end up [2 effect, blue]
effect:
  2: on fire
  duplicated
  as a plant pot
blue: blue`
})

export const mutations = {
  updateDice(state, value) {
    state.dice = value
  },
  updateCode(state, value) {
    state.code = value
  }
}